<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OpeningPosition extends Model
{
    use SoftDeletes;

    protected $table = 'opening_positions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'location',
        'event_url',
        'number_slot',
    ];

    protected $dates = ['deleted_at'];

    protected $casts = [
        'created_by' => 'integer',
        'number_slot' => 'integer',
    ];
}
