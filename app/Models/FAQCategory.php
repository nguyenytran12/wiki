<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FAQ;

class FAQCategory extends Model
{
    use SoftDeletes;

    protected $table = 'faq_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
    ];

    protected $dates = ['deleted_at'];

    protected $casts = [
        'created_by' => 'integer',
    ];

    public function faqs(): BelongsToMany
    {
        return $this->belongsToMany(FAQ::class);
    }
}
