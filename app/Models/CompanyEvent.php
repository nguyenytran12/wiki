<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyEvent extends Model
{
    use SoftDeletes;

    protected $table = 'company_events';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'pic_url',
        'event_url',
        'description',
        'start_time',
        'end_time'
    ];

    protected $dates = ['deleted_at'];

    protected $casts = [
        'created_by' => 'integer'
    ];
}
