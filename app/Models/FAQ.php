<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FAQCategory;

class FAQ extends Model
{
    use SoftDeletes;

    protected $table = 'faqs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question_content',
        'answer_content',
    ];

    protected $dates = ['deleted_at'];

    protected $casts = [
        'created_by' => 'integer',
    ];

    public function faqCategories(): BelongsToMany
    {
        return $this->belongsToMany(FAQCategory::class);
    }
}
