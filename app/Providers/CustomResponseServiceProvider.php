<?php


namespace App\Providers;

use Exception;
use Laravel\Lumen\Http\ResponseFactory;
use Illuminate\Support\ServiceProvider;

class CustomResponseServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerCustomResponse();
    }

    /**
     * Register custom response
     *
     * @return void
     */
    private function registerCustomResponse()
    {
        ResponseFactory::macro('success', function ($message = '', $data = [], $statusCode = 200, $headers = []) {
            return response()->json([
                'status_code' => $statusCode,
                'message' => $message,
                'data' => $data,
            ], $statusCode, $headers);
        });

        ResponseFactory::macro('error', function ($message, $error, $statusCode = 400, $headers = []) {
            if ($error instanceof Exception) {
                \Log::error($error);
                $error = $error->getMessage();
            }

            return response()->json([
                'status_code' => $statusCode,
                'message' => $message,
                'errors' => $error,
            ], $statusCode, $headers);
        });

        ResponseFactory::macro('notfound', function ($message, $statusCode = 404, $headers = []) {
            return response()->json([
                'status_code' => $statusCode,
                'message' => $message,
            ], $statusCode, $headers);
        });
    }
}
