<?php


namespace App\Repositories;

use App\Repositories\Contracts\BaseRepository;
use \Illuminate\Database\Query\Builder;
use Prettus\Repository\Eloquent\BaseRepository as Repository;

abstract class AbstractEloquentRepository extends Repository implements BaseRepository
{
    /**
     * @inheritdoc
     */
    public function findBy(array $searchCriteria = [], \Closure $builder = null, $paginate = true)
    {
        $limit = !empty($searchCriteria['limit']) ? (int)$searchCriteria['limit'] : 15; // it's needed for pagination
        $filter = !empty($searchCriteria['filter']) ? (array)$searchCriteria['filter'] : [];
        $sort = !empty($searchCriteria['sort']) ? (string)$searchCriteria['sort'] : '';

        $queryBuilder = $this->model->where(function ($query) use ($filter, $sort) {
            $this->applySearchCriteriaInQueryBuilder($query, $filter);
        });

        $this->applySortingInQueryBuilder($queryBuilder, $sort);

        if (is_callable($builder)) {
            $builder($queryBuilder);
        }

        if ($paginate) {
            return $queryBuilder->paginate($limit);
        }

        return $queryBuilder->get();
    }

    /**
     * Apply condition on query builder based on search criteria
     *
     * @param Object $queryBuilder
     * @param array $searchCriteria
     * @return mixed
     */
    protected function applySearchCriteriaInQueryBuilder($queryBuilder, array $searchCriteria = [])
    {

        foreach ($searchCriteria as $key => $value) {
            // skip pagination related query params and ambiguous fields
            if (in_array($key, ['page', 'per_page', 'limit', 'deleted_at'])) {
                continue;
            }

            //we can pass multiple params for a filter with commas
            $allValues = explode(',', $value);

            if (count($allValues) > 1) {
                $queryBuilder->whereIn($key, $allValues);
            } else {
                $operator = '=';
                $queryBuilder->where($key, $operator, $value);
            }
        }

        return $queryBuilder;
    }

    /**
     * Apply condition on query builder based on search criteria
     *
     * @param Builder $queryBuilder
     * @param null $sortString
     * @return mixed
     */
    protected function applySortingInQueryBuilder(Builder $queryBuilder, $sortString = null)
    {
        $sortFields = explode(',', $sortString);
        if (count($sortFields) > 0) {
            foreach ($sortFields as $field) {
                if (empty($field)) {
                    continue;
                }
                if (strpos($field, '-') === 0) {
                    $field = substr($field, 1);
                    if ($field) {
                        $queryBuilder->orderByDesc($field);
                    }
                } else {
                    $queryBuilder->orderBy($field);
                }
            }
        }

        return $queryBuilder;
    }
}
