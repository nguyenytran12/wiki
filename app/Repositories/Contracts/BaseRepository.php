<?php

namespace App\Repositories\Contracts;

interface BaseRepository
{
    /**
     * Search All resources by criteria
     *
     * @param array $searchCriteria
     * @param \Closure|null $builder
     * @param bool $paginate
     * @return mixed
     */
    public function findBy(array $searchCriteria = [], \Closure $builder = null, $paginate = true);
}
