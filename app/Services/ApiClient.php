<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Log\Logger;
use InvalidArgumentException;
use \Illuminate\Support\Str;

/**
 * @method object get($uri, ...$args)
 * @method object post($uri, ...$data)
 * @method object put($uri, ...$data)
 * @method object patch($uri, ...$data)
 * @method object head($uri, ...$data)
 * @method object delete($uri)
 */
abstract class ApiClient
{
    protected $responseFormat = 'json';

    protected $client;

    protected $logger;


    public function __construct(Client $client, Logger $logger = null)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $params
     * @return mixed|\SimpleXMLElement|string
     */
    public function request(string $method, string $uri, array $params = [])
    {
        try {
            $body = (string) $this->getClient()
                ->$method($this->buildUrl($uri), ['form_params' => $params])
                ->getBody();

            if ($this->responseFormat === 'json') {
                return json_decode($body);
            }

            if ($this->responseFormat === 'xml') {
                return simplexml_load_string($body);
            }

            return $body;
        } catch (ClientException $e) {
            $this->logger->error($e);
        }
    }

    /**
     * @param string $uri
     * @return string
     */
    public function buildUrl(string $uri): string
    {
        if (!Str::startsWith($uri, ['http://', 'https://'])) {
            if ($uri[0] !== '/') {
                $uri = "/$uri";
            }

            $uri = $this->getEndpoint().$uri;
        }

        return $uri;
    }

    /**
     * Make an HTTP call to the external resource.
     *
     * @param string  $method The HTTP method
     * @param mixed[] $args   An array of parameters
     *
     * @throws InvalidArgumentException
     *
     * @return mixed|null|SimpleXMLElement
     */
    public function __call(string $method, array $args)
    {
        if (count($args) < 1) {
            throw new InvalidArgumentException('Magic request methods require a URI and optional options array');
        }

        $uri = $args[0];
        $opts = isset($args[1]) ? $args[1] : [];

        return $this->request($method, $uri, $opts);
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    abstract public function getEndpoint(): ?string;
}
