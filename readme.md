## Requirement
1. PHP >= 7.2 with native redis extension installed.
2. Redis
3. MySQL 5.7 with `utf8mb4` character sets  
  
## Development with docker

1. **Installation**
    - Clone this repository and change working directory into this project
    - Setup environment variables:
        ```sh
        cp .env.example .env
        ```
    - Composer install:
        ```
        composer install
        ```
    - Migration:
        ```
        php artisan migrate --seed
        ```  
_Enjoy!_
