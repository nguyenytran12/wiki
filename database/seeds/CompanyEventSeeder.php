<?php

use Illuminate\Database\Seeder;
use App\Models\CompanyEvent;

class CompanyEventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(CompanyEvent::class, 10)->create();
    }
}
