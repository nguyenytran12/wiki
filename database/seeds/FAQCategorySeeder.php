<?php

use App\Models\FAQ;
use Illuminate\Database\Seeder;
use App\Models\FAQCategory;

class FAQCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(FAQCategory::class, 10)->create();

        $fags = factory(FAQ::class, 10)->create();

        FAQCategory::all()->each(function (FAQCategory $faqCategory) use ($fags) {
            $faqCategory->faqs()->saveMany($fags);
        });
    }
}
