<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(\CompanyEventSeeder::class);

        $this->call(\OpeningPositionSeeder::class);

        $this->call(\FAQCategorySeeder::class);

        $this->call(\QuoteSeeder::class);
    }
}
