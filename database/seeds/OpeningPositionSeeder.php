<?php

use Illuminate\Database\Seeder;
use App\Models\OpeningPosition;
use App\Models\OpeningPositionCV;

class OpeningPositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(OpeningPosition::class, 10)->create()->each(function ($item) {
            factory(OpeningPositionCV::class)->create([
                'openning_positions_id' => $item->id
            ]);
        });
    }
}
