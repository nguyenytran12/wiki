<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\OpeningPositionCV::class, function (Faker\Generator $faker) {
    return [
       'status' => 0,
       'openning_positions_id' => 1
    ];
});
